import 'package:sonocare_patient_app/background.dart';
import 'package:sonocare_patient_app/ui_kits/normalButton.dart';
import 'package:sonocare_patient_app/ui_kits/snack_bar.dart';
import 'package:sonocare_patient_app/ui_kits/textField.dart';
import 'package:sonocare_patient_app/utill/color_resources.dart';
import 'package:sonocare_patient_app/utill/images.dart';
import 'package:flutter/material.dart';

class LoginScreen extends StatefulWidget {
  String email = "";
  String password = "";
  LoginScreen({this.email="", this.password=""});
  @override
  __SignInFormState createState() => __SignInFormState();
}

class __SignInFormState extends State<LoginScreen> {
  //final _controller = Get.put(LoginController());

  final GlobalKey<FormState> _key = GlobalKey<FormState>();
  final _passwordController = TextEditingController();
  final _emailController = TextEditingController();
  bool _autoValidate = false;

  List<String> accountTypeList = ['Doctor', 'Nurse'];
  String selectedAccount = 'Doctor';


  @override
  Widget build(BuildContext context) {
  // TODO: implement build
    if(widget.email.isNotEmpty){
      _emailController.text=widget.email;
      _passwordController.text=widget.password;
    }
  return Stack(
    children: [
      /*
            Obx(() {
              return
            }),
            */
      BackGround(),
      Scaffold(
        backgroundColor: Colors.transparent,
        body: Form(
          key: _key,
          autovalidateMode:
          _autoValidate ? AutovalidateMode.always : AutovalidateMode.disabled,
          child: ListView(
            children: [
              const SizedBox(height: 0,),
              Image.asset(Images.main_logo_sm, height: 200, fit: BoxFit.contain,),
              const SizedBox(height: 0,),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20.0),
                child: textField(hintText: 'Enter your Email', label:'Email', icon: const Icon(Icons.mail, color: Colors.white,), controller: _emailController, validator: (){}, onChanged: (){},),
              ),
              const SizedBox(height: 20,),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20.0),
                child: textField(obscureText: true, hintText: 'Enter your Password', label:'Password', icon: const Icon(Icons.lock, color: Colors.white), controller: _passwordController, validator: (){}, onChanged: (){}),
              ),
              const SizedBox(height: 20,),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20.0),
                child: Column(
                  children: [
                    Row(
                      children: [
                        const Padding(
                          padding: EdgeInsets.all(12.0),
                          child: Icon(Icons.person, color: Colors.transparent,),
                        ),
                        SizedBox(width: 1,child: Container(color: Colors.white,), height: 64,),
                        Expanded(
                          child: Padding(
                            padding: const EdgeInsets.only(left: 15.0, bottom: 0),
                            child: Stack(
                              children: [
                                const Text('Account Type', textAlign: TextAlign.start, style: TextStyle(fontSize: 14, fontWeight: FontWeight.w400, color: Colors.white),),
                                Padding(
                                  padding: const EdgeInsets.only(top:10.0),
                                  child: DropdownButton<String>(
                                    isExpanded: true,
                                    dropdownColor: ColorResources.COLOR_PURPLE_DEEP,
                                    underline: Container(color: Colors.transparent),
                                    items: accountTypeList.map((String accountType) {
                                      return DropdownMenuItem<String>(
                                        value: accountType,
                                        child: Text(accountType, style: const TextStyle(color: Colors.white),),
                                      );
                                    }).toList(),
                                    onChanged: (value){
                                      setState(() {
                                        selectedAccount = value!;
                                      });
                                    },
                                    value: selectedAccount,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(width: MediaQuery.of(context).size.width,child: Container(color: Colors.white,), height: 1,),
                  ],
                ),
              ),
              const SizedBox(height: 10,),
              GestureDetector(
                  onTap: (){
                    print('forget password');
                  },
                  child: const Padding(
                    padding: EdgeInsets.all(20.0),
                    child: Text('Forgot Password?', textAlign: TextAlign.center, style: TextStyle(color: Colors.white, fontSize: 14),),
                  )),
              const SizedBox(height: 10,),
              Padding(
                padding: const EdgeInsets.only(left: 40.0, right: 40, top: 10, bottom: 10),
                child: Row(
                  children: [
                    Expanded(child: normalButton(
                        backgroundColor: ColorResources.COLOR_PURPLE_MID,
                        button_text: 'Log in',
                        primaryColor: ColorResources.COLOR_WHITE,
                        fontSize: 16,
                        onTap: () async {
                          if(_emailController.text.split('@').length==2){
                            print('090090909');
                          }else{
                            showCustomSnackBar('Invalid email address', context, isError: true);
                          }
                        }
                    )),
                  ],
                ),
              ),
              const SizedBox(height: 20,),
              GestureDetector(
                onTap: (){},
                child: const Padding(
                  padding: EdgeInsets.all(18.0),
                  child: Text('Don\'t have an account? Sign Up', textAlign: TextAlign.center, style: TextStyle(color: Colors.white, fontSize: 14),),
                ),
              ),
              const SizedBox(height: 40,),

            ],),
        ),
      ),
    ],
  );
  }

  _onLoginButtonPressed(){}
  }
