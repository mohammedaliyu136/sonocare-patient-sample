class MessageModel{
  String title = '';
  String body = '';

  MessageModel({required this.title, required this.body});

  MessageModel.fromJson(Map<String, dynamic> json) {
    title = json['title'];
    body = json['body'];
  }

  Map<String, String>? toJson() {
    final Map<String, String>? json = Map<String, String>();
    json!['title'] = title;
    json['body'] = body;
    return json;
  }
}