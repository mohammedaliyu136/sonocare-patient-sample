import 'dart:async';
import 'dart:convert';

import 'package:agora_rtc_engine/rtc_engine.dart';
import 'package:awesome_notifications/android_foreground_service.dart';
import 'package:awesome_notifications/awesome_notifications.dart';
import 'package:sonocare_patient_app/calling/calling.dart';
import 'package:sonocare_patient_app/utill/app_constants.dart';
import 'package:sonocare_patient_app/utill/color_resources.dart';
import 'package:sonocare_patient_app/utill/common_functions.dart';
import 'package:sonocare_patient_app/utill/single_slider.dart';
//import 'package:sonocare_patient_app/views/calling/JoinChannelVideo.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:vibration/vibration.dart';

import 'package:agora_rtc_engine/rtc_local_view.dart' as RtcLocalView;
import 'package:agora_rtc_engine/rtc_remote_view.dart' as RtcRemoteView;

import 'package:http/http.dart' as http;

class PhoneCallPage extends StatefulWidget {

  final ReceivedAction receivedAction;

  PhoneCallPage({Key? key, required this.receivedAction}) : super(key: key);

  @override
  State<PhoneCallPage> createState() => _PhoneCallPageState();
}

class _PhoneCallPageState extends State<PhoneCallPage> {
  //---------------------------------------
  bool _localUserJoined = false;
  bool _showStats = false;
  int? _remoteUid;
  late RtcEngine engine;
  //RtcStats _stats = RtcStats();
  bool activeVideo = true;
  bool activeAudio = true;
  bool acceptedCall = false;
  //---------------------------------------
  //String token = '006da2e58ec2ef84ca29aa5d23c7523fb82IADC8yDEYTzARFpM8D1Ruf37r/DB173CNPNqlbLg7MG6zoZ79fQAAAAAEAB6xVtdQjgCYgEAAQDS9ABi';
  //String channel = 'doctor12';
  //---------------------------------------

  Timer? _timer;
  Duration _secondsElapsed = Duration.zero;

  void startCallingTimer() {
    setState(() {
      acceptedCall = true;
    });
    const oneSec = Duration(seconds: 1);
    AndroidForegroundService.stopForeground();

    //initForAgora();
    String channel = widget.receivedAction.payload!['channel']??'';
    //print(channel);
    String doc_uid = "21";
    bool videoCall = widget.receivedAction.payload!['allowVideo']=='true'?true:false;
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) =>
          CallingScreen(channel: channel, remoteUID: '', videoCall:videoCall)
        //JoinChannelVideo()
      ),
    );

      _timer = new Timer.periodic(
      oneSec, (Timer timer) {
          setState(() {
            _secondsElapsed += oneSec;
          });
      },
    );
  }

  void finishCall(){
    Vibration.vibrate(duration: 100);
    AndroidForegroundService.stopForeground();
    Navigator.pop(context);
  }

  Future<void> initForAgora() async {
    // retrieve permissions
    await [Permission.microphone, Permission.camera].request();

    // create the engine for communicating with agora
    engine = await RtcEngine.create(AppConstants.appId);

    // set up event handling for the engine
    engine.setEventHandler(RtcEngineEventHandler(
      joinChannelSuccess: (String channel, int uid, int elapsed) {
        print('$uid successfully joined channel: $channel ');
        setState(() {
          _localUserJoined = true;
        });
      },
      userJoined: (int uid, int elapsed) {
        print('remote user $uid joined channel');
        setState(() {
          _remoteUid = uid;
        });
      },
      userOffline: (int uid, UserOfflineReason reason) {
        print('remote user $uid left channel');
        setState(() {
          _remoteUid = null;
        });
      },
    ));
    // enable video
    await engine.enableVideo();

    await engine.joinChannel(
      null,
          'doc12345', null, 0);
  }

  @override
  void initState() {
    lockScreenPortrait();
    super.initState();
    if(widget.receivedAction.buttonKeyPressed == 'ACCEPT'){
      acceptedCall =  true;
      startCallingTimer();
      //initForAgora();
    }else{
      acceptedCall =  false;
    }
  }

  @override
  void dispose() {
    _timer?.cancel();
    unlockScreenPortrait();
    AndroidForegroundService.stopForeground();
    super.dispose();
  }


  @override
  Widget build(BuildContext context) {

    MediaQueryData mediaQueryData = MediaQuery.of(context);
    ThemeData themeData = Theme.of(context);

    return Scaffold(
      body: AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle.light,
        child: Stack(
          fit: StackFit.expand,
          children: [
            /*
            // Image
            Image(
              image: widget.receivedAction.largeIconImage!,
              fit: BoxFit.cover,
            ),
            // Black Layer
            const DecoratedBox(
              decoration: BoxDecoration(color: Colors.black45),
            ),
            */
            const DecoratedBox(
              decoration: BoxDecoration(color: ColorResources.COLOR_PURPLE_MID),
            ),
            Padding(
              padding: const EdgeInsets.all(20.0),
              child: SafeArea(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    SizedBox(height: 100,),
                    Text(
                      widget.receivedAction.payload?['username']?.replaceAll(r'\s+', r'\n')
                          ?? 'Unknow',
                      maxLines: 4,
                      style: themeData
                          .textTheme
                          .headline5
                          ?.copyWith(color: Colors.white),
                    ),
                    Text(
                      _timer == null ?
                      'Incoming call' : 'Please wait',//'Call in progress: ${printDuration(_secondsElapsed)}',
                      style: themeData
                          .textTheme
                          .headline6
                          ?.copyWith(color: Colors.white54, fontSize: _timer == null ? 20 : 12),
                    ),
                    const SizedBox(height: 50),
                    /*
                    _timer == null ?
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        TextButton(
                            onPressed: (){},
                            style: ButtonStyle(
                              overlayColor: MaterialStateProperty.all<Color>(Colors.white12),
                            ),
                            child: Column(
                              children: [
                                const Icon(FontAwesomeIcons.solidClock, color: Colors.white54),
                                Text('Reminder me', style:  themeData
                                    .textTheme
                                    .headline6
                                    ?.copyWith(color: Colors.white54, fontSize: 12, height: 2))
                              ],
                            )
                        ),
                        const SizedBox(),
                        TextButton(
                          onPressed: (){},
                          style: ButtonStyle(
                            overlayColor: MaterialStateProperty.all<Color>(Colors.white12),
                          ),
                          child: Column(
                            children: [
                              const Icon(FontAwesomeIcons.solidEnvelope, color: Colors.white54),
                              Text('Message', style:  themeData
                                  .textTheme
                                  .headline6
                                  ?.copyWith(color: Colors.white54, fontSize: 12, height: 2))
                            ],
                          ),
                        )
                      ],
                    ) : const SizedBox(),
                    */
                    const Spacer(),
                    Container(
                      padding: const EdgeInsets.all(15),
                      decoration: const BoxDecoration(
                        color: Colors.black54,
                        borderRadius: BorderRadius.all(Radius.circular(45)),
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: _timer == null ?
                        [
                          RoundedButton(
                            press: finishCall,
                            color: Colors.red,
                            icon: const Icon(FontAwesomeIcons.phoneAlt, color: Colors.white),
                          ),
                          SingleSliderToConfirm(
                            onConfirmation: (){
                              Vibration.vibrate(duration: 100);
                              startCallingTimer();
                            },
                            width: mediaQueryData.size.width * 0.55,
                            backgroundColor: Colors.white60,
                            text: 'Slide to Talk',
                            stickToEnd: true,
                            textStyle: Theme.of(context)
                                .textTheme
                                .headline6
                                ?.copyWith(color: Colors.white, fontSize: mediaQueryData.size.width * 0.05),
                            sliderButtonContent: RoundedButton(
                              press: (){},
                              color: Colors.white,
                              icon: const Icon(FontAwesomeIcons.phoneAlt, color: Colors.green),
                            ),
                          )
                        ] :[SizedBox()]
                        /*
                        [
                          RoundedButton(
                            press: (){},
                            icon: const Icon(FontAwesomeIcons.microphone),
                          ),
                          RoundedButton(
                            press: finishCall,
                            color: Colors.red,
                            icon: const Icon(FontAwesomeIcons.phoneAlt, color: Colors.white),
                          ),
                          RoundedButton(
                            press: (){},
                            icon: const Icon(FontAwesomeIcons.volumeUp),
                          ),
                        ],
                        */
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
    }
  // remote user video
  Widget _renderRemoteVideo() {
    if (_remoteUid != null) {
      return RtcRemoteView.SurfaceView(uid: _remoteUid??0);
    } else {
      return Text(
        'Please wait for patient to join',
        textAlign: TextAlign.center,
      );
    }
  }
  // current user video
  Widget _renderLocalPreview() {
    if (_localUserJoined) {
      return RtcLocalView.SurfaceView();
    } else {
      return Text(
        'Joining Chat',
        textAlign: TextAlign.center,
      );
    }
  }
}

class RoundedButton extends StatelessWidget {
  const RoundedButton({
    Key? key,
    this.size = 64,
    required this.icon,
    this.color = Colors.white,
    required this.press,
  }) : super(key: key);

  final double size;
  final Icon icon;
  final Color color;
  final VoidCallback press;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: size,
      width: size,
      child: FlatButton(
        padding: EdgeInsets.all(15 / 64 * size),
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(100)),
        ),
        color: color,
        onPressed: press,
        child: icon,
      ),
    );
  }
}