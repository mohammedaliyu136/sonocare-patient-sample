import 'package:flutter/material.dart';
import 'package:sonocare_patient_app/ui_kits/normalButton.dart';
import 'package:sonocare_patient_app/utill/color_resources.dart';

class WhotoCallPage extends StatelessWidget {
  WhotoCallPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 40.0, right: 40, top: 10, bottom: 10),
            child: Row(
              children: [
                Expanded(child: normalButton(
                    backgroundColor: ColorResources.COLOR_PURPLE_MID,
                    button_text: 'Call Doctor',
                    primaryColor: ColorResources.COLOR_WHITE,
                    fontSize: 16,
                    onTap: () async {}
                )),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
