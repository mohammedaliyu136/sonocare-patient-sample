import 'dart:convert';

import 'package:awesome_notifications/android_foreground_service.dart';
import 'package:awesome_notifications/awesome_notifications.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sonocare_patient_app/calling/calling.dart';
import 'package:sonocare_patient_app/calling/phone_call_page.dart';
import 'package:sonocare_patient_app/messageModel.dart';
import 'package:sonocare_patient_app/provider/call_provider.dart';
import 'package:sonocare_patient_app/utill/notification_util.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

import 'package:http/http.dart' as http;


Future<void> firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  print("Handling a background message");
  //saveNot(message);
  //alertSound();
  if(message.data['title']=='call'){
    NotificationUtils.showCallNotification(325, userName: message.data['body'], allowVideo: message.data['allowVideo'], channel:message.data['channel']);
  }

  if(message.data['title']=='end-call'){
    await NotificationUtils.cancelAllNotifications();
    await NotificationUtils.dismissAllNotifications();
    await AndroidForegroundService.stopForeground();
  }
}

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();

  FirebaseMessaging messaging;
  messaging = FirebaseMessaging.instance;
  messaging.getToken().then((value){
    print('(*)(*)(*)(*)');
    print(value);
  });
  FirebaseMessaging.onBackgroundMessage(firebaseMessagingBackgroundHandler);
  AwesomeNotifications().initialize(
      'resource://drawable/res_app_icon',
      [
        NotificationChannel(
            channelGroupKey: 'category_tests',
            channelKey: 'call_channel',
            channelName: 'Calls Channel',
            channelDescription: 'Channel with call ringtone',
            defaultColor: const Color(0xFF9D50DD),
            importance: NotificationImportance.Max,
            ledColor: Colors.white,
            channelShowBadge: true,
            locked: true,
            defaultRingtoneType: DefaultRingtoneType.Ringtone),
      ]
  );
  runApp(
      MultiProvider(
          providers: [
            ChangeNotifierProvider(create: (context) => CallProvider()),
          ],
          child: const MyApp())
  );
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;
  String doc_uid = '';
  String doc_token = '';
  String MYToken = '';

  Future<bool> _incrementCounter({videoCall=true}) async {
    //String _token = 'dhpKmzLkRKCbc8B0KJ-FBM:APA91bH6OCRaehVzgz81PBh-Q8QniQzGejTuvvs7fii9z0oe7K4dUY3UKwvKL0zUgC9G2PyhomhRgmwIevltm3SLFWqC4ZyRDzIv3YuTkBFz60ATtQ-Y8xcnCcnOFh52tkf5vU3ULP8T';

    String _token = doc_token;
    try {
      await http.post(
        Uri.parse('https://fcm.googleapis.com/fcm/send'),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          'Authorization': 'key=AAAA4unw2EQ:APA91bGM5R6U7HTz096YExo_ktdW3zTFePeXtpvh88GWTT4rbFQvj49KQHmZpiq8-qmslwk_ZFysdeBnXRmdb3mSpo66wct6U2OA4zssEb8EUn6KCgYMUKJ0xn-rByFNy66FnaADu7Q5'
        },
        body: constructFCMPayload(_token, videoCall:videoCall),
      ).then((value){
        print(value.statusCode);
        //CallingScreen
        String channel = "doctor_55";
        //print(channel);
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) =>
              CallingScreen(channel: channel, remoteUID: doc_uid, remoteToken: doc_token, localToken: MYToken, madeTheCall: true, videoCall:videoCall)
            //JoinChannelVideo()
          ),
        );

      });
      print('FCM request for device sent!');
    } catch (e) {
      print(e);
    }
    return true;
  }
  String constructFCMPayload(String? token, {videoCall=true}) {
    print('00000000000111');
    print(token!);

    return jsonEncode({
      'to': token,
      "notification" : {
        "body" : "You have a call from John Doe",
        "title": "Call"
      },
      "data" : {
        "body" : "John Doe",
        "title": "call",
        "type": "call",
        "allowVideo":"${videoCall}"
      }
    });
  }

  @override
  void initState() {
    Provider.of<CallProvider>(context, listen: false).initialization(context);
    FirebaseFirestore.instance
        .collection('f_users_token').doc('doc_55').get().then((value){
          doc_uid = 'doc_55';
          doc_token = value.data()!['f_token'];
    });

    FirebaseMessaging.onMessage.listen((RemoteMessage message) {

      if(message.data['type']=='call') {
        print('caall');
        NotificationUtils.showCallNotification(1, userName: message.data['body'], allowVideo: message.data['allowVideo'], channel:message.data['channel']);
      }
      if(message.data['type']=='message') {
        NotificationUtils.showInboxNotification(1, message.data['body']);
      }
    });
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const Text(
              'call doctor',
            ),
            Text(
              '$_counter',
              style: Theme.of(context).textTheme.headline4,
            ),
          ],
        ),
      ),
      floatingActionButton: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          FloatingActionButton(
            onPressed: ()=>_incrementCounter(videoCall: false),
            tooltip: 'Increment',
            child: const Icon(Icons.wifi_calling),
          ),
          const SizedBox(height: 8,),
          FloatingActionButton(
            onPressed: ()=>_incrementCounter(),
            tooltip: 'Increment',
            child: const Icon(Icons.video_call),
          ),
        ],
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }

  void getMyToken()async{
    FirebaseMessaging messaging;
    messaging = FirebaseMessaging.instance;
    messaging.getToken().then((myToken){
      print(myToken);
      setState(() {
        MYToken = myToken!;
      });
    });
  }
}
