import 'package:awesome_notifications/android_foreground_service.dart';
import 'package:awesome_notifications/awesome_notifications.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sonocare_patient_app/calling/phone_call_page.dart';

class CallProvider with ChangeNotifier {

  late BuildContext _context;

  String token = '';
  String remoteUID = '';
  String channel = '';

  initialization(BuildContext context) async {
    AwesomeNotifications().actionStream.listen((receivedAction) {

      if(receivedAction.channelKey == 'call_channel'){
        switch (receivedAction.buttonKeyPressed) {

          case 'REJECT':
            AndroidForegroundService.stopForeground();
            break;

          case 'ACCEPT':
            loadSingletonPage(receivedAction: receivedAction);
            AndroidForegroundService.stopForeground();
            break;

          default:
            loadSingletonPage(receivedAction: receivedAction);
            break;
        }
        return;
      }

    });
    _context = context;
    String _token = "";
    FirebaseMessaging messaging;
    messaging = FirebaseMessaging.instance;
    await messaging.getToken().then((value) {
      print('(*)(*)(*)(*)');
      print(value);
      _token = value!;
    });
    try{
      FirebaseFirestore.instance
          .collection('f_users_token')
          .doc('pat_55').set({'f_token':_token, 'email':"pat@sonocare.com", 'time':FieldValue.serverTimestamp()});
    }catch (e) {
      print(e);
    }

  }

  void loadSingletonPage({required ReceivedAction receivedAction}){
    receivedAction.buttonKeyPressed = '';
    Navigator.push(
      _context!,
      MaterialPageRoute(
          builder: (context) =>
              PhoneCallPage(receivedAction: receivedAction,)),
    );
  }


}